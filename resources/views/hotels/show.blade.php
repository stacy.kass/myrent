@php
    $startDate = request()->get('start_date', \Carbon\Carbon::now()->format('Y-m-d'));
    $endDate = request()->get('end_date', \Carbon\Carbon::now()->addDay()->format('Y-m-d'));
@endphp

<x-app-layout>
    <div class="py-14 px-4 md:px-6 2xl:px-20 2xl:container 2xl:mx-auto">
        <div class="flex flex-wrap mb-12">
            <div class="w-full flex justify-start md:w-1/3 mb-8 md:mb-0">
                <img class="h-full rounded-l-sm" src="{{ asset($hotel->photo) }}" alt="Room Image">
            </div>
            <div class="w-full md:w-2/3 px-4">
                <div class="text-2xl font-bold">{{ $hotel->title }}</div>
                <div class="flex items-center">
                    {{--<x-gmdi-pin-drop-o class="w-5 h-5 mr-1 text-blue-700"/>--}}
                    {{ $hotel->address }}
                </div>
                <div>{{ $hotel->description }}</div>
            </div>
        </div>
        <div class="flex flex-col">
            <div class="text-2xl text-center md:text-start font-bold">Забронировать комнату</div>

            <form id="filterForm" method="get" action="{{ url()->current() }}">
                <div class="flex flex-col my-6">
                    <div class="flex items-center mb-5">
                        <div class="relative">
                            <input name="start_date" min="{{ date('Y-m-d') }}" value="{{ $startDate }}"
                                   placeholder="Дата заезда" type="date"
                                   class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5">
                        </div>
                        <span class="mx-4 text-gray-500">по</span>
                        <div class="relative">
                            <input name="end_date" type="date" min="{{ date('Y-m-d') }}" value="{{ $endDate }}"
                                   placeholder="Дата выезда"
                                   class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5">
                        </div>
                    </div>
                    <div class="flex items-center py-4 mb-5">
                        <div class="relative">
                            <input name="minimum_price" min="{{ date('Y-m-d') }}" value="{{ request('minimum_price') }}"
                                   placeholder="Минимальная цена" type="number"
                                   class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5">
                        </div>
                        <div class="relative mx-4">
                            <input name="maximum_price" type="number" min="{{ date('Y-m-d') }}" value="{{ request('maximum_price') }}"
                                   placeholder="Максимальная цена"
                                   class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5">
                        </div>
                        <div class="flex items-center mb-5">
                            <label class="flex items-center mr-5">
                                <input type="checkbox" name="facilities[]" value="1" class="form-checkbox text-blue-500" {{ in_array('1', request()->input('facilities', [])) ? 'checked' : '' }}>
                                <span class="ml-1 text-gray-700">Wi-Fi</span>
                            </label>
                            <label class="flex items-center mr-5">
                                <input type="checkbox" name="facilities[]" value="2" class="form-checkbox text-blue-500" {{ in_array('2', request()->input('facilities', [])) ? 'checked' : '' }}>
                                <span class="ml-1 text-gray-700">Кондиционер</span>
                            </label>
                            <label class="flex items-center mr-5">
                                <input type="checkbox" name="facilities[]" value="3" class="form-checkbox text-blue-500" {{ in_array('3', request()->input('facilities', [])) ? 'checked' : '' }}>
                                <span class="ml-1 text-gray-700">Телевизор</span>
                            </label>
                            <label class="flex items-center mr-5">
                                <input type="checkbox" name="facilities[]" value="4" class="form-checkbox text-blue-500" {{ in_array('4', request()->input('facilities', [])) ? 'checked' : '' }}>
                                <span class="ml-1 text-gray-700">Бесплатный завтрак</span>
                            </label>
                        </div>
                    </div>
                    <div class="flex">
                        <x-the-button class="mr-5" type="submit" class="">Загрузить номера</x-the-button>
                        {{--<x-the-button type="reset" id="resetButton">Сбросить фильтр</x-the-button>--}}
                    </div>
                </div>
            </form>

            @if(request()->filled('start_date') && request()->filled('end_date'))
                <div class="flex flex-col w-full lg:w-4/5">
                    @foreach($rooms as $room)
                        <x-rooms.room-list-item :room="$room" class="mb-4"/>
                    @endforeach
                </div>
            @else
                <div></div>
            @endif
        </div>
    </div>
</x-app-layout>
<script>
    document.getElementById('resetButton').addEventListener('click', function() {
        document.getElementById('filterForm').reset(); // Сброс данных формы
    });
</script>
