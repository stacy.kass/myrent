<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Уведомление о бронировании отеля</title>
</head>
<body style="font-family: Arial, sans-serif;">

<h2>Уведомление о бронировании отеля</h2>

<p>Уважаемый гость,</p>

<p>Ваше бронирование в отеле <strong>{{ $booking->room->hotel->title }}</strong> успешно создано.</p>

<p>Даты бронирования: {{ $booking->started_at }} - {{ $booking->finished_at }}</p>

<p>Цена: {{ $booking->price }} рублей</p>

<p>Спасибо за ваше бронирование! Пожалуйста, свяжитесь с нами, если у вас возникнут какие-либо вопросы.</p>

<p>С уважением,<br>
    MyRent</p>

</body>
</html>
