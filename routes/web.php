<?php

use App\Http\Controllers\BookingsController;
use App\Http\Controllers\HotelsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoomsController;
use App\Orchid\Screens\BookingsScreen;
use App\Orchid\Screens\FacilitiesScreen;
use App\Orchid\Screens\HotelsScreen;
use App\Orchid\Screens\RoomsScreen;
use Illuminate\Support\Facades\Route;


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {

    Route::get('/', function () {
        return view('index');
    });

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('/hotels', HotelsController::class);
    Route::resource('/bookings', BookingsController::class);

    Route::screen('/admin/bookings', BookingsScreen::class)->name('platform.bookings');
    Route::screen('/admin/hotels', HotelsScreen::class)->name('platform.hotels');
    Route::screen('/admin/rooms', RoomsScreen::class)->name('platform.rooms');
    Route::screen('/admin/facilities', FacilitiesScreen::class)->name('platform.facilities');
});

require __DIR__.'/auth.php';



