<?php

namespace App\Orchid\Screens;

use App\Models\Facilities;
use App\Models\FacilityHotel;
use App\Models\Hotel;
use App\Orchid\Layouts\HotelsListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class HotelsScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'hotels' => Hotel::all(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Отели';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Создать'))
                ->icon('bs.plus-circle')
                ->route('platform.systems.hotels.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            HotelsListLayout::class,
        ];
    }

    public function remove(Request $request): void
    {
        Hotel::findOrFail($request->get('id'))->delete();

        Toast::info(__('Отель удален'));
    }
}
