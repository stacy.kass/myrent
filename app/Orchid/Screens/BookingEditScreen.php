<?php

namespace App\Orchid\Screens;

use App\Models\Booking;
use App\Orchid\Layouts\BookingEditLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class BookingEditScreen extends Screen
{
    public $booking;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Booking $booking)
    {
        return [
            'booking' => $booking,
        ];
    }


    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->booking->exists ? 'Редактирование бронирования' : 'Создание бронирования';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(BookingEditLayout::class)
                ->title(__('Параметры бронирования'))
                ->description(__('Все поля являются обязательными'))
                ->commands(
                    Button::make(__('Сохранить'))
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        //->canSee($this->booking->exists)
                        ->method('save')
                ),
        ];
    }

    public function save(Booking $booking, Request $request)
    {
/*        $request->validate([
            'booking.price' => 'required',
            'booking.started_at' => 'required',
            'booking.finished_at' => 'required',
        ]);*/

        $booking->fill($request->collect('booking')->toArray())->save();

        Toast::info(__('Бронирование сохранено.'));

        return redirect()->route('platform.bookings');
    }
}
