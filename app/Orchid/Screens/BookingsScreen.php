<?php

namespace App\Orchid\Screens;

use App\Models\Booking;
use App\Models\Room;
use App\Orchid\Layouts\BookingFiltersLayout;
use App\Orchid\Layouts\BookingsListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class BookingsScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'bookings' => Booking::with('room')->filters(BookingFiltersLayout::class)->defaultSort('price')->paginate(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Бронирования';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Создать'))
                ->icon('bs.plus-circle')
                ->route('platform.systems.bookings.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            BookingsListLayout::class,
        ];
    }

    public function remove(Request $request): void
    {
        Booking::findOrFail($request->get('id'))->delete();

        Toast::info(__('Бронирование удалено'));
    }
}
