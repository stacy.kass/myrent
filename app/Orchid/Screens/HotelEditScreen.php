<?php

namespace App\Orchid\Screens;


use App\Models\Facilities;
use App\Models\FacilityHotel;
use App\Models\Hotel;
use App\Orchid\Layouts\HotelEditLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class HotelEditScreen extends Screen
{
    public $hotel;
    public $facility;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Hotel $hotel)
    {
        return [
            'hotel' => $hotel,
        ];
    }


    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->hotel->exists ? 'Редактирование отеля' : 'Создание отеля';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(HotelEditLayout::class)
                ->title(__('Параметры отеля'))
                ->description(__('После добавления данных нажмите кнопку "cохранить"'))
                ->commands(
                    Button::make(__('Сохранить'))
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        //->canSee($this->booking->exists)
                        ->method('save')
                ),
        ];
    }

    public function save(Hotel $hotel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hotel.title' => 'required|max:100',
            'hotel.poster_url' => 'max:100',
            'hotel.address' => 'required|max:500',
        ], [
            'hotel.title.required' => 'Поле название обязательно для заполнения',
            'hotel.title.max' => 'Текст не должен превышать :max знаков',
            'hotel.poster_url.max' => 'Ссылка не должна быть длиннее :max знаков',
            'hotel.address.required' => 'Поле адреса обязательно для заполнения',
            'hotel.address.max' => 'Максимальная длина адреса :max символов',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $hotel->fill($request->collect('hotel')->toArray())->save();

        Toast::info(__('Отель сохранен.'));

        return redirect()->route('platform.hotels');
    }
}
