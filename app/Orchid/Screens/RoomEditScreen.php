<?php

namespace App\Orchid\Screens;


use App\Models\Room;
use App\Orchid\Layouts\HotelEditLayout;
use App\Orchid\Layouts\RoomEditLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class RoomEditScreen extends Screen
{
    public $room;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Room $room)
    {
        return [
            'room' => $room,
        ];
    }


    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->room->exists ? 'Редактирование номера' : 'Создание номера';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(RoomEditLayout::class)
                ->title(__('Номера'))
                ->description(__('Все поля являются обязательными'))
                ->commands(
                    Button::make(__('Сохранить'))
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        //->canSee($this->booking->exists)
                        ->method('save')
                ),
        ];
    }

    public function save(Room $room, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room.title' => 'required|max:100',
            'room.poster_url' => 'max:100',
            'room.type' => 'required|max:100',
            'room.price' => 'required|integer',
        ], [
            'room.title.required' => 'Поле название обязательно для заполнения',
            'room.title.max' => 'Текст не должен превышать :max знаков',
            'room.poster_url.max' => 'Ссылка не должна быть длиннее :max знаков',
            'room.floor_area.required' => 'Поле адреса обязательно для заполнения',
            'room.floor_area.integer' => 'Поле площади должно быть числом',
            'room.type.required' => 'Поле тип обязательно для заполнения',
            'room.type.max' => 'Тип отеля не должен превышать :max знаков',
            'room.price.required' => 'Поле цена обязательно для заполнения',
            'room.price.integer' => 'Поле цены должно быть числом',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $room->fill($request->collect('room')->toArray())->save();

        Toast::info(__('Номер сохранен.'));

        return redirect()->route('platform.rooms');
    }
}
