<?php

namespace App\Orchid\Screens;


use App\Models\Facilities;
use App\Models\FacilityHotel;
use App\Models\FacilityRoom;
use App\Models\Hotel;
use App\Orchid\Layouts\FacilitiesEditLayout;
use App\Orchid\Layouts\RoomEditLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FacilitiesEditScreen extends Screen
{
    public $facility;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Facilities $facility)
    {
        return [
            'facility' => $facility,
        ];
    }


    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->facility->exists ? 'Редактирование удобства' : 'Создание удобства';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(FacilitiesEditLayout::class)
                ->title(__('Удобства'))
                ->description(__('Все поля являются обязательными'))
                ->commands(
                    Button::make(__('Сохранить'))
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        //->canSee($this->booking->exists)
                        ->method('save')
                ),
        ];
    }

    public function save(Facilities $facility, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'facility.title' => 'required|max:100',
        ], [
            'facility.title.required' => 'Поле название обязательно для заполнения',
            'facility.title.max' => 'Название не должно превышать :max знаков',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $request = $request->get('facility');

        $facility->fill([
            'title' => $request['title'],
        ])->save();

        $facilityId = $facility->id;
        $hotelIds = $request['hotel'];
        $roomIds = $request['room'];

        $existingHotelIds = FacilityHotel::where('facility_id', $facilityId)
            ->pluck('hotel_id')
            ->toArray();

        $existingRoomIds = FacilityRoom::where('facility_id', $facilityId)
            ->pluck('room_id')
            ->toArray();

        if (empty($hotelIds)) {
            FacilityHotel::where('facility_id', $facilityId)->delete();
        } else {
            $hotelsToAdd = array_diff($hotelIds, $existingHotelIds);
            $hotelsToRemove = array_diff($existingHotelIds, $hotelIds);

            foreach ($hotelsToAdd as $hotelId) {
                FacilityHotel::create([
                    'hotel_id' => $hotelId,
                    'facility_id' => $facilityId,
                ]);
            }
            FacilityHotel::where('facility_id', $facilityId)
                ->whereIn('hotel_id', $hotelsToRemove)
                ->delete();
        }

        if (empty($roomIds)) {
            FacilityRoom::where('facility_id', $facilityId)->delete();
        } else {
            $roomsToAdd = array_diff($roomIds, $existingRoomIds);
            $roomsToRemove = array_diff($existingRoomIds, $roomIds);

            foreach ($roomsToAdd as $roomId) {
                FacilityRoom::create([
                    'room_id' => $roomId,
                    'facility_id' => $facilityId,
                ]);
            }
            FacilityRoom::where('facility_id', $facilityId)
                ->whereIn('room_id', $roomsToRemove)
                ->delete();
        }


        Toast::info(__('Удобство сохранено.'));

        return redirect()->route('platform.facilities');
    }
}
