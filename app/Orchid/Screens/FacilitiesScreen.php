<?php

namespace App\Orchid\Screens;

use App\Orchid\Layouts\FacilitiesListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use App\Models\Facilities;
use Orchid\Support\Facades\Toast;

class FacilitiesScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'facilities' => Facilities::with('hotel', 'room')->get(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Удобства';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Создать'))
                ->icon('bs.plus-circle')
                ->route('platform.systems.facilities.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            FacilitiesListLayout::class,
        ];
    }

    public function remove(Request $request): void
    {
        Facilities::findOrFail($request->get('id'))->delete();

        Toast::info(__('Удобство удалено'));
    }
}
