<?php

namespace App\Orchid\Layouts;

use App\Models\Facilities;
use App\Models\Hotel;
use App\Models\Room;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class FacilitiesEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */

    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('facility.title')
                ->required()
                ->title(__('Название'))
                ->placeholder(__('Название')),

            Relation::make('facility.hotel')
            ->fromModel(Hotel::class, 'title')
            ->multiple()
                ->title('Отель'),

            Relation::make('facility.room')
                ->fromModel(Room::class, 'title')
                ->multiple()
                ->title('Номер'),
        ];
    }
}
