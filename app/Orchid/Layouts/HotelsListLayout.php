<?php

namespace App\Orchid\Layouts;

use App\Models\Hotel;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class HotelsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'hotels';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('photo', 'Фотография отеля')
                ->render(function (Hotel $hotel) {
                    return '<img src="' . $hotel->photo . '" style="max-width: 200px; max-height: 200px;">';
                }),
            TD::make('id','id')
                ->sort(),
            TD::make('title','Название отеля'),
            TD::make('description','Описание')
                ->defaultHidden(),
            TD::make('poster_url','Ссылка'),
            TD::make('address','Адрес'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Hotel $hotel) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([

                        Link::make(__('Редактировать'))
                            ->route('platform.hotel.edit', $hotel)
                            ->icon('bs.pencil'),

                        Button::make(__('Удалить'))
                            //->route('hotels.destroy', $hotel)
                            ->icon('bs.trash3')
                            ->confirm(__('Вы уверены что хотите удалить отель? Это действие нельзя будет отменить.'))
                        ->method('remove', [
                            'id' => $hotel->id,
                        ]),
                    ])),
        ];
    }
}
