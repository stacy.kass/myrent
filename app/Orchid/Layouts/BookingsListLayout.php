<?php

namespace App\Orchid\Layouts;

use App\Models\Booking;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Components\Cells\DateTimeSplit;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class BookingsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'bookings';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */

    protected function columns(): iterable
    {
        return [
            TD::make('id', 'id')
            ->sort()
                ->filter(Input::make()),
            TD::make('price', 'Цена (руб)')
            ->sort(),
            TD::make('room.title', __('Номер'))
                ->sort(),
            TD::make('Пользователь')
                ->filter(Input::make())
                ->render(function ($booking) {
                    return $booking->user->email;
                })
                ->sort(),
            TD::make('created_at', 'Дата создания')
                ->usingComponent(DateTimeSplit::class)
                ->sort(),
            TD::make('started_at', 'Check in')
                ->usingComponent(DateTimeSplit::class)
                ->sort(),
            TD::make('finished_at', 'Check out')
                ->usingComponent(DateTimeSplit::class)
                ->sort(),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Booking $booking) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([

                        Link::make(__('Редактировать'))
                            ->route('platform.booking.edit', $booking)
                            ->icon('bs.pencil'),

                        Button::make(__('Удалить'))
                            ->icon('bs.trash3')
                            ->confirm(__('Вы уверены что хотите удалить бронирование? Это действие нельзя будет отменить.'))
                            ->method('remove', [
                                'id' => $booking->id,
                            ]),
                    ])),
        ];
    }
}
