<?php

namespace App\Orchid\Layouts;

use App\Models\Facilities;
use App\Models\FacilityHotel;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class HotelEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */

    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('hotel.title')
                ->required()
                ->title(__('Название'))
                ->placeholder(__('Название')),

            TextArea::make('hotel.description')
                ->title('Описание')
                ->rows(5),

            Input::make('hotel.poster_url')
                ->title('Ссылка')
                ->placeholder(__('Ссылка')),

            TextArea::make('hotel.address')
                ->required()
                ->title('Адрес')
                ->rows(3),

            Cropper::make('hotel.photo')
                ->title('Фотография отеля')
                ->targetRelativeUrl()
                ->width(1000)
                ->height(1000),
        ];
    }
}
