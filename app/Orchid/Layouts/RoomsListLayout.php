<?php

namespace App\Orchid\Layouts;

use App\Models\Hotel;
use App\Models\Room;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class RoomsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'rooms';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('photo', 'Фотография номера')
                ->render(function (Room $room) {
                    return '<img src="' . $room->photo . '" style="max-width: 200px; max-height: 200px;">';
                }),
            TD::make('id', 'id')
                ->sort(),
            TD::make('title', 'Название'),
            TD::make('description', 'Описание'),
            TD::make('poster_url', 'Ссылка'),
            TD::make('floor_area', 'Площадь'),
            TD::make('price', 'Цена (руб)')
            ->sort(),
            TD::make('Отель')
                ->render(function ($rooms) {
                    return $rooms->hotel->title;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Room $room) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([

                        Link::make(__('Редактировать'))
                            ->route('platform.room.edit', $room)
                            ->icon('bs.pencil'),

                        Button::make(__('Удалить'))
                            //->route('rooms.destroy', $room)
                            ->icon('bs.trash3')
                            ->confirm(__('Вы уверены что хотите удалить номер? Это действие нельзя будет отменить.'))
                        ->method('remove', [
                            'id' => $room->id,
                        ]),
                    ])),
        ];
    }
}
