<?php

namespace App\Orchid\Layouts;

use App\Models\Facilities;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FacilitiesListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'facilities';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id','id')
            ->sort(),
            TD::make('title','Название'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Facilities $facility) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([

                        Link::make(__('Редактировать'))
                            ->route('platform.facility.edit', $facility)
                            ->icon('bs.pencil'),

                        Button::make(__('Удалить'))
                            //->route('facilities.destroy', $facility)
                            ->icon('bs.trash3')
                            ->confirm(__('Вы уверены что хотите удалить удобство? Это действие нельзя будет отменить.'))
                         ->method('remove', [
                             'id' => $facility->id,
                         ]),
                    ])),
        ];
    }
}
