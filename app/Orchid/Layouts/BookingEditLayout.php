<?php

namespace App\Orchid\Layouts;

use App\Models\Booking;
use App\Models\Room;
use App\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class BookingEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */

    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('booking.price')
                ->type('number')
                ->required()
                ->title(__('Цена'))
                ->placeholder(__('Цена')),

            DateTimer::make('booking.started_at')
                ->required()
                ->title('Check in')
                ->format('Y-m-d'),

            DateTimer::make('booking.finished_at')
                ->required()
                ->title('Check out')
                ->format('Y-m-d'),

            Relation::make('booking.room_id')
                ->fromModel(Room::class, 'title')
                ->title('Комната'),

            Relation::make('booking.user_id')
                ->fromModel(User::class, 'email')
                ->title('Пользователь'),
        ];
    }
}
