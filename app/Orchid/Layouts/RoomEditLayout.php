<?php

namespace App\Orchid\Layouts;

use App\Models\Hotel;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class RoomEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */

    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('room.title')
                ->required()
                ->title(__('Название'))
                ->placeholder(__('Название')),

            TextArea::make('room.description')
                ->title('Описание')
                ->rows(5),

            Input::make('room.floor_area')
                ->required()
                ->title('Площадь')
                ->type('number')
                ->placeholder(__('Площадь')),

            Input::make('room.poster_url')
                ->title('Ссылка')
                ->placeholder(__('Ссылка')),

            Input::make('room.type')
                ->required()
                ->title('Тип'),

            Input::make('room.price')
                ->required()
                ->type('number')
                ->title('Стоимость за сутки'),

            Relation::make('room.hotel_id')
                ->fromModel(Hotel::class, 'title')
                ->required()
                ->title('Отель'),

            Cropper::make('room.photo')
                ->title('Фотография номера')
                ->targetRelativeUrl()
                ->width(1000)
                ->height(1000),
        ];
    }
}
