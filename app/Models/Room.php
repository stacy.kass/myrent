<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Room extends Model
{
    use HasFactory, AsSource;

    protected $fillable = [
        'id',
        'title',
        'description',
        'poster_url',
        'floor_area',
        'type',
        'price',
        'hotel_id',
        'photo',
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function facilities()
    {
        return $this->belongsToMany(Facilities::class,
            'facility_room',
            'room_id',
            'facility_id',
        );
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
