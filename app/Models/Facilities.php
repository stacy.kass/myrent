<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Facilities extends Model
{
    use HasFactory, AsSource;
    protected $fillable = [
        'id',
        'title',
    ];

    public function hotel()
    {
        return $this->belongsToMany(Hotel::class,
            'facility_hotel',
            'facility_id',
            'hotel_id',
        );
    }

    public function room()
    {
        return $this->belongsToMany(Room::class,
            'facility_room',
            'facility_id',
            'room_id',
        );
    }
}
