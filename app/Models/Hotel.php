<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Hotel extends Model
{
    use HasFactory, AsSource;

    protected $fillable = [
        'id',
        'title',
        'description',
        'poster_url',
        'address',
        'photo'
    ];

    public function facilities()
    {
        return $this->belongsToMany(Facilities::class,
        'facility_hotel',
            'hotel_id',
            'facility_id',
        );
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
