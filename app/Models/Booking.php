<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Filters\Types\Like;
use Orchid\Filters\Types\Where;
use Orchid\Filters\Types\WhereDateStartEnd;
use Orchid\Screen\AsSource;

class Booking extends Model
{
    use HasFactory, AsSource, Filterable;
    protected $fillable =[
        'id',
        'room_id',
        'user_id',
        'started_at',
        'finished_at',
        'price',
    ];

    protected array $allowedSorts = [
        'room_id',
        'user_id',
        'started_at',
        'finished_at',
        'price',
        'created_at',
        'id',
    ];

    protected $allowedFilters = [
        'id'            => Where::class,
        'user_id'       => Like::class,
        'created_at'    => WhereDateStartEnd::class,
        'started_at'    => WhereDateStartEnd::class,
        'finished_at'    => WhereDateStartEnd::class,
    ];

    public static $rules = [
        'room_id' => 'required|exists:rooms,id',
        'user_id' => 'required|exists:users,id',
        'started_at' => 'required|date|before_or_equal:finished_at',
        'finished_at' => 'required|date|after_or_equal:started_at',
        'price' => 'required|numeric|min:0',
    ];

    public function room()
    {
        return $this->hasOne(Room::class,
        'id',
            'room_id'
        );
    }

    public function user()
    {
        return $this->hasOne(User::class,
            'id',
            'user_id'
        );
    }
}
