<?php

namespace App\Http\Controllers;

use App\Mail\BookingCompletedMailing;
use App\Models\Booking;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class BookingsController extends Controller
{
    public function index()
    {
        $bookings = Booking::where('user_id', Auth::id())->get()->map(function ($booking) {
            foreach ($booking as $item) {
                $booking->days = $this->totalDays(Carbon::parse($booking->started_at), Carbon::parse($booking->finished_at));
            }
            return $booking;
        });

        return view('bookings.index')->with('bookings', $bookings);
        //return response()->json($booking, 200);
    }

    public function show(Booking $booking)
    {
        $booking->days = $this->totalDays(Carbon::parse($booking->started_at), Carbon::parse($booking->finished_at));
        return view('bookings.show')->with('booking', $booking);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room_id' => 'required|exists:rooms,id',
            'user_id' => 'required|exists:users,id',
            'started_at' => 'required|date|before_or_equal:finished_at',
            'finished_at' => 'required|date|after_or_equal:started_at',
            'price' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['errors' => $errors->toJson()], 422);
        }

        $booking = Booking::create([
            'room_id' => $request->get('room_id'),
            'user_id' => $request->get('user_id'),
            'started_at' => $request->get('started_at'),
            'finished_at' => $request->get('finished_at'),
            'price' => $request->get('price'),
        ]);

        Mail::to($request->user())->send(new BookingCompletedMailing($booking));

        $booking->days = $this->totalDays(
            Carbon::parse($request->get('started_at')),
            Carbon::parse($request->get('finished_at'))
        );

        //return response()->json($booking, 201);
        return redirect('/bookings/'.$booking->id);
    }

    public function destroy(Booking $booking)
    {
        $booking->delete();

        return redirect('/bookings');
    }

    public function totalDays($startDate, $endDate)
    {
        return round($startDate->diffInDays($endDate));
    }

}
