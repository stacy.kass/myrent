<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class RoomsController extends Controller
{
   public function index()
   {
       return Room::all();
   }

   public function show(Room $room)
   {
        return $room;
   }

   public function store(Request $request)
   {
       $room = Room::create([
           'title' => $request->get('title'),
           'description' => $request->get('description'),
           'poster_url' => $request->get('poster_url'),
           'floor_area' => $request->get('floor_area'),
           'type' => $request->get('type'),
           'price' => $request->get('price'),
           'hotel_id' => $request->get('hotel_id'),
       ]);
       return response()->json($room, 201);
   }

   public function update(Request $request, Room $room)
   {
       $room->update($request->all());

       return response()->json($room, 203);
   }

   public function destroy(Room $room)
   {
       $room->delete();

       return response()->json([], 204);
   }
}
