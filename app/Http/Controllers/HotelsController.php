<?php
// $room->total_days
// $room->total_price
namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Facilities;
use App\Models\Hotel;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HotelsController extends Controller
{
    public function index()
    {

        $hotels = Hotel::all()->map(function ($hotel) {
            foreach ($hotel as $item) {
                $hotel->price = $hotel->rooms->min('price');
            }

            return $hotel;
        });


        return view('hotels.index')->with('hotels', $hotels);
    }

    public function show(Hotel $hotel, Request $request)
    {
        $rooms = $hotel->rooms()->get();

        if ($request->has('start_date') && $request->has('end_date')) {

            $startDate = Carbon::parse($request->get('start_date'));
            $endDate = Carbon::parse($request->get('end_date'));

            $total_days = $startDate->diffInDays($endDate);

            if ($total_days == 0) {
                $total_days += 1;
            }

            foreach ($rooms as $room) {
                $isBooked = Booking::where('room_id', $room->id)
                    ->where(function ($query) use ($startDate, $endDate) {
                        $query->where('started_at', '<=', $startDate)
                            ->where('finished_at', '>=', $startDate);
                    })
                    ->orWhere(function ($query) use ($startDate, $endDate) {
                        $query->where('started_at', '<=', $endDate)
                            ->where('finished_at', '>=', $endDate);
                    })
                    ->orWhere(function ($query) use ($startDate, $endDate) {
                        $query->where('started_at', '>=', $startDate)
                            ->where('finished_at', '<=', $endDate);
                    })
                    ->exists();

                $room->total_days = $total_days;
                $room->total_price = $room->price * $total_days;

                if ($isBooked) {
                    $rooms->forget($room->id);
                }
            }
        } else {
            $rooms = $hotel->rooms()->get();
        }

        $filteredRooms = $rooms->when($request->input('minimum_price'), function ($collection, $minPrice) {
            return $collection->filter(function ($room) use ($minPrice) {
                return $room->price >= $minPrice;
            });
        })
            ->when($request->input('maximum_price'), function ($collection, $maxPrice) {
                return $collection->filter(function ($room) use ($maxPrice) {
                    return $room->price <= $maxPrice;
                });
            })
            ->when($request->has('facilities'), function ($collection) use ($request) {
                $selectedFacilities = $request->input('facilities', []);
                return $collection->filter(function ($room) use ($selectedFacilities) {
                    foreach ($selectedFacilities as $facilityId) {
                        foreach ($room->facilities as $facilitiy)
                        if ($facilitiy->id == $facilityId) {
                            return $room;
                        }
                    }
                    return false;
                });
            });

        return view('hotels.show')->with([
            'hotel' => $hotel,
            'rooms' => $filteredRooms,
        ]);
    }

    public function store(Request $request)
    {
        $hotel = Hotel::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'poster_url' => $request->get('poster_url'),
            'address' => $request->get('address'),
        ]);

        return response()->json($hotel, 201);
    }

    public function update(Request $request, Hotel $hotel)
    {
        $hotel->update($request->all());

        return response()->json($hotel, 203);
    }

    public function destroy(Hotel $hotel)
    {
        $hotel->delete();

        return response()->json([], 204);
    }
}
