# Проект по бронированию отелей на Laravel 11

Добро пожаловать в проект по бронированию отелей, созданный на Laravel. В этом проекте реализованы функции регистрации и авторизации пользователей, бронирования отелей, просмотра и отмены бронирований, а также рассылка уведомлений о новых бронированиях на электронную почту. Система поддерживает различные роли пользователей, такие как администратор и обычный пользователь. Для администраторов отелей предусмотрена админ-панель, где можно управлять бронированиями, удобствами, отелями, номерами и пользователями.

## Технологии

- **Laravel 11**
- **MySQL**
- **PHP 8.3**
- **Orchid Admin Panel**
- **Blade шаблоны**

## Установка

1. **Создание базы данных**

   Создайте базу данных для проекта на вашем сервере MySQL.

2. **Клонирование репозитория**

   Склонируйте репозиторий проекта на ваш локальный компьютер:
   ```sh
   git clone https://gitlab.com/stacy.kass/myrent
   cd myrent
   ```

3. **Настройка .env файла**

   Скопируйте файл `.env.example` в `.env` и настройте его под свои нужды:
   ```sh
   cp .env.example .env
   ```

4. **Установка зависимостей**

   Установите зависимости композера и npm:
   ```sh
   composer install
   npm install
   ```

5. **Генерация ключа приложения**

   Сгенерируйте ключ приложения:
   ```sh
   php artisan key:generate
   ```

6. **Миграции и сиды**

   Выполните миграции для создания таблиц в базе данных:
   ```sh
   php artisan migrate
   ```

7. **Создание администратора системы**

   Создайте администратора системы с помощью Orchid:
   ```sh
   php artisan orchid:admin admin admin@admin.com password
   ```

8. **Сборка ассетов**

   Соберите фронтенд ассеты:
   ```sh
   npm run dev
   ```

9. **Запуск сервера**

   Запустите локальный сервер для разработки:
   ```sh
   php artisan serve
   ```

10. **Настройка почтовой службы**

    Настройте почтовую службу для отправки уведомлений о бронированиях в файле `.env`:
    ```
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS="noreply@example.com"
    MAIL_FROM_NAME="${APP_NAME}"
    ```

## Админ-панель

Админ-панель доступна по пути `/admin`. В админ-панели администраторы могут:
- Просматривать статистику бронирований
- Создавать, редактировать и удалять бронирования, удобства, отели и номера
- Управлять пользователями, назначать роли и редактировать их данные

## Примененные навыки

В процессе создания этого проекта я применила различные навыки:
- Разработка на PHP 8.3 с использованием фреймворка Laravel 11
- Проектирование и управление базами данных MySQL
- Разработка и интеграция админ-панелей с использованием Orchid
- Шаблонизация с помощью Blade
- Настройка и управление почтовыми службами для отправки уведомлений
- Работа с миграциями и сидами для базы данных
- Работа с системой контроля версий Git

## Лицензия

Проект является авторским, и без моего разрешения запрещено копировать или использовать его код в других проектах.

---

♥ Этот проект создан с любовью и вниманием к деталям. Если у вас есть вопросы или предложения, пожалуйста, свяжитесь со мной. Спасибо за внимание!
